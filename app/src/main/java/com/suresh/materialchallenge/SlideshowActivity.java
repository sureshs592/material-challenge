package com.suresh.materialchallenge;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.suresh.materialchallenge.sample.content.ImageFragment;
import com.suresh.materialchallenge.utils.DepthPageTransformer;

import java.util.ArrayList;

/**
 * Created by suresh on 28/3/15.
 */
public class SlideshowActivity extends ActionBarActivity {

    public static final String KEY_URLS = "urls";
    public static final String KEY_LEFT = "left";
    public static final String KEY_TOP = "top";
    public static final String KEY_WIDTH = "width";
    public static final String KEY_HEIGHT = "height";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slideshow);

        String[] imageUrls = getIntent().getStringArrayExtra(KEY_URLS);

        ViewPager viewPager = (ViewPager) findViewById(R.id.slideshowViewPager);
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        SlideshowAdapter adapter = new SlideshowAdapter(getSupportFragmentManager(), imageUrls, getIntent().getExtras());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);

//        setupImmersiveMode(viewPager); Disabled to make activity transition work smoothly
    }

    public void setupImmersiveMode(View view) {
        int controls = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) { //API 16 and above. Mostly immersive

            controls = controls
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_FULLSCREEN;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { //API 19 and obove. Real immersive mode
                controls |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            }

        } else { //Below API 16. Supports the legacy immersive mode.
            controls |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
        }

        view.setSystemUiVisibility(controls);
    }

    public static class SlideshowAdapter extends FragmentPagerAdapter {

        private String[] urls;
        private Bundle transitionData;

        public SlideshowAdapter(FragmentManager fMgr, String[] urls, Bundle transitionData) {
            super(fMgr);
            this.urls = urls;
            this.transitionData = transitionData;
        }

        @Override
        public Fragment getItem(int position) {
            Bundle args = (position == 0) ? transitionData : null;
            ImageFragment frag = ImageFragment.newInstance(urls[position], args);
            return frag;
        }

        @Override
        public int getCount() {
            return urls.length;
        }
    }
}
