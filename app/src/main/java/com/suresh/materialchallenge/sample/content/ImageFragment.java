package com.suresh.materialchallenge.sample.content;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.suresh.materialchallenge.R;
import com.suresh.materialchallenge.SlideshowActivity;

import java.util.List;

/**
 * Created by suresh on 28/3/15.
 */
public class ImageFragment extends Fragment {

    private static final String KEY_URL = "url";
    private static final String KEY_TRANSITION_DATA = "transition_data";
    private static final int TRANSITION_DURATION = 400;

    private String imageUrl;
    private Bundle transitionData;

    private ImageView image;
    private ProgressBar progressBar;

    public static ImageFragment newInstance(String imageUrl, Bundle transitionData) {
        ImageFragment frag = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(KEY_URL, imageUrl);
        if (transitionData != null) args.putBundle(KEY_TRANSITION_DATA, transitionData);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUrl = getArguments().getString(KEY_URL);

        if (getArguments().containsKey(KEY_TRANSITION_DATA)) {
            transitionData = getArguments().getBundle(KEY_TRANSITION_DATA);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);

        image = (ImageView) view.findViewById(R.id.image);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        //Check if there will be a transition or a normal display
        if (transitionData != null) {
            ViewTreeObserver observer = image.getViewTreeObserver();
            observer.addOnPreDrawListener(new ActivityTransition());
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<Bitmap> cachedImgs = MemoryCacheUtils.findCachedBitmapsForImageUri(imageUrl,
                ImageLoader.getInstance().getMemoryCache());

        if (cachedImgs.isEmpty()) {
            ImageLoader.getInstance().displayImage(imageUrl, image, new NetworkImageListener());
        } else {
            image.setImageBitmap(cachedImgs.get(0));
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    public class ActivityTransition implements ViewTreeObserver.OnPreDrawListener {

        @Override
        public boolean onPreDraw() {
            image.getViewTreeObserver().removeOnPreDrawListener(this);

            int[] coordinates = new int[2];
            image.getLocationOnScreen(coordinates);
            int leftDelta = transitionData.getInt(SlideshowActivity.KEY_LEFT) - coordinates[0];
            int topDelta = transitionData.getInt(SlideshowActivity.KEY_TOP) - coordinates[1];

            float widthScale = (float) transitionData.getInt(SlideshowActivity.KEY_WIDTH) / image.getWidth();
            float heightScale = (float) transitionData.getInt(SlideshowActivity.KEY_HEIGHT) / image.getHeight();

            runZoomTransition(leftDelta, topDelta, widthScale, heightScale);
            return true;
        }
    }

    private void runZoomTransition(int leftDelta, int topDelta, float widthScale, float heightScale) {


        image.setPivotX(0);
        image.setPivotY(0);
        image.setScaleX(widthScale);
        image.setScaleY(heightScale);
        image.setTranslationX(leftDelta);
        image.setTranslationY(topDelta);

        image.animate().setInterpolator(new DecelerateInterpolator()).setDuration(TRANSITION_DURATION)
                .scaleX(1f).scaleY(1f)
                .translationX(0f).translationY(0f);

        View background = getActivity().findViewById(R.id.slideshowViewPager);
        background.setAlpha(0f);
        background.animate().setInterpolator(new DecelerateInterpolator()).setDuration(TRANSITION_DURATION)
                .alpha(1f);
//        ObjectAnimator bgAnimator = ObjectAnimator.ofInt(background, "alpha", 0, 255);
//        bgAnimator.setDuration(TRANSITION_DURATION);
//        bgAnimator.start();
    }

    public class TransitionListener implements Animator.AnimatorListener {

        @Override public void onAnimationStart(Animator animation) { }

        @Override
        public void onAnimationEnd(Animator animation) {
            View background = getActivity().findViewById(R.id.slideshowViewPager);
            ((SlideshowActivity)getActivity()).setupImmersiveMode(background);
        }

        @Override public void onAnimationCancel(Animator animation) { }

        @Override public void onAnimationRepeat(Animator animation) { }
    }

    public class NetworkImageListener implements ImageLoadingListener {

        @Override public void onLoadingStarted(String imageUri, View view) { }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            progressBar.setVisibility(View.INVISIBLE);
            //TODO: Display broken image placeholder
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            ((ImageView)view).setImageBitmap(loadedImage);
            progressBar.setVisibility(View.INVISIBLE);
        }

        @Override public void onLoadingCancelled(String imageUri, View view) { }
    }
}
