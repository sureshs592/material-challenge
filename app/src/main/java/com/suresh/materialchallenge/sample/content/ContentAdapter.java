package com.suresh.materialchallenge.sample.content;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.suresh.materialchallenge.R;
import com.suresh.materialchallenge.SlideshowActivity;
import com.suresh.materialchallenge.utils.Data;

import java.util.ArrayList;

/**
 * Created by suresh on 25/3/15.
 */
public class ContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ClickListener clickListener;
    private ArrayList<Data> sampleData;
    private Drawable userProfilePlaceholder;
    private NetworkImageListener imageLoadListener;
    private int lastAnimatedPosition;

    public ContentAdapter(Context context, ArrayList<Data> sampleData, ClickListener clickListener) {
        this(context, sampleData, clickListener, -1);
    }

    public ContentAdapter(Context context, ArrayList<Data> sampleData, ClickListener clickListener, int lastAnimatedPosition) {
        this.context = context;
        this.clickListener = clickListener;
        this.sampleData = sampleData;
        this.lastAnimatedPosition = lastAnimatedPosition;

        preparePlaceholderImage();
        imageLoadListener = new NetworkImageListener();
    }

    private void preparePlaceholderImage() {
        Resources res = context.getResources();
        Bitmap bm = BitmapFactory.decodeResource(res, R.drawable.user_profile_placeholder);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(res, bm);
        drawable.setCornerRadius(Math.max(bm.getWidth(), bm.getHeight()) / 2.0f);
        userProfilePlaceholder = drawable;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == R.id.type_post) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sample_item_1,
                    viewGroup, false);
            return new PostCard(itemView);
        } else {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sample_item_2,
                    viewGroup, false);
            return new BannerCard(itemView, clickListener);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        int itemType = getItemViewType(i);
        boolean shouldAnimateEntry = i > lastAnimatedPosition;
        if (shouldAnimateEntry) lastAnimatedPosition = i;

        if (itemType == R.id.type_post) { //Post
            PostCard card = (PostCard) viewHolder;
            Data.PostData data = (Data.PostData) sampleData.get(i);
            bindPostContent(card, data, shouldAnimateEntry);
        } else { //Banner
            BannerCard card = (BannerCard) viewHolder;
            Data.BannerData data = (Data.BannerData) sampleData.get(i);
            bindBannerContent(card, data, shouldAnimateEntry);
        }
    }

    private void bindPostContent(PostCard card, Data.PostData data, boolean shouldAnimate) {
        String currentImgUrl = (String) card.imgUserProfile.getTag();

        if (currentImgUrl == null || !data.profileImgUrl.equals(currentImgUrl)) {
            ImageLoader.getInstance().displayImage
                    (data.profileImgUrl, card.imgUserProfile, imageLoadListener);
        }

        if (shouldAnimate) animateViewEntry(card.container);
    }

    private void bindBannerContent(BannerCard card, Data.BannerData data, boolean shouldAnimate) {
        String currentImgUrl = (String) card.imgBanner.getTag();
        String newImageUrl = data.bannerImgUrls[0];

        if (currentImgUrl == null || !newImageUrl.equals(currentImgUrl)) {
            ImageLoader.getInstance().displayImage
                    (newImageUrl, card.imgBanner);
            card.imgBanner.setTag(newImageUrl);
        }

        if (shouldAnimate) animateViewEntry(card.container);
    }

    private void animateViewEntry(View view) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.card_entry_anim);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return sampleData.size();
    }

    public ArrayList<Data> getData() {
        return sampleData;
    }

    public int getLastAnimatedPosition() {
        return lastAnimatedPosition;
    }

    @Override
    public int getItemViewType(int position) {
        if (sampleData.get(position) instanceof Data.PostData) {
            return R.id.type_post;
        } else {
            return R.id.type_banner;
        }
    }



    /**
     * ViewHolder for the post card
     */
    public static class PostCard extends RecyclerView.ViewHolder {

        View container;
        ImageView imgUserProfile;

        public PostCard(View itemView) {
            super(itemView);
            container = itemView;
            imgUserProfile = (ImageView) itemView.findViewById(R.id.imgUserProfile);
        }
    }

    /**
     * ViewHolder for the banner card. Invokes the listener for banner image clicks.
     */
    public static class BannerCard extends RecyclerView.ViewHolder implements View.OnClickListener {
        View container;
        ImageView imgBanner;
        ClickListener listener;

        public BannerCard(View itemView, ClickListener listener) {
            super(itemView);
            container = itemView;
            imgBanner = (ImageView) itemView.findViewById(R.id.imgBanner);
            imgBanner.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onBannerClick(getAdapterPosition(), (ImageView) v);
        }


    }

    /**
     * Listener for the profile image. Mainly used to round the corners of the network image once it's received.
     * <br />
     * <b>Start:</b> Set the placeholder drawable <br />
     * <b>Complete:</b> Round the corners of the received bitmap and display it in the ImageView
     */
    public class NetworkImageListener implements ImageLoadingListener {

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            ((ImageView)view).setImageDrawable(userProfilePlaceholder);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            Resources res = context.getResources();
            RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(res, loadedImage);
            drawable.setCornerRadius(Math.max(loadedImage.getWidth(), loadedImage.getHeight()) / 2.0f);
            ((ImageView)view).setImageDrawable(drawable);
            view.setTag(imageUri);
        }

        @Override public void onLoadingFailed(String imageUri, View view, FailReason failReason) { }

        @Override public void onLoadingCancelled(String imageUri, View view) { }
    }
}

/**
 * Interface to receive banner image clicks.
 */
interface ClickListener {
    /**
     * Called when the image is clicked
     * @param position Adapter position for the respective ViewHolder
     * @param img ImageView that's been clicked
     */
    public void onBannerClick(int position, ImageView img);
}