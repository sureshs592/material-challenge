package com.suresh.materialchallenge.sample.content;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.suresh.materialchallenge.R;
import com.suresh.materialchallenge.SlideshowActivity;
import com.suresh.materialchallenge.utils.Data;
import com.suresh.materialchallenge.utils.SampleDataGenerator;

import java.util.ArrayList;

/**
 * Created by suresh on 25/3/15.
 */
public class ContentFragment extends Fragment implements ClickListener {

    RecyclerView recyclerView;
    ContentAdapter adapter;
    ListScrollListener scrollListener;

    private final static String KEY_DATA = "data";
    private final static String KEY_LAST_ANIMATED_POS = "last_animated_pos";

    public interface ListScrollListener {
        public void onScroll(RecyclerView recyclerView, int dx, int dy);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sample, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setOnScrollListener(new ContentScrollListener());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) { //Creating a fresh adapter
            ArrayList<Data> data = new SampleDataGenerator(20).getList();
            adapter = new ContentAdapter(getActivity(), data, this);
        } else { //Creating an adapter based on information stored in the bundle (used during screen orientation changes)
            ArrayList<Data> data = (ArrayList<Data>) savedInstanceState.getSerializable(KEY_DATA);
            int lastAnimatedPos = savedInstanceState.getInt(KEY_LAST_ANIMATED_POS);
            adapter = new ContentAdapter(getActivity(), data, this, lastAnimatedPos);
        }
        recyclerView.setAdapter(adapter);

        //Set the scroll listener to control the activity header size + toolbar transparency
        if (getActivity() instanceof ListScrollListener) {
            scrollListener = (ListScrollListener) getActivity();
        }

        setupLayoutManager();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_DATA, adapter.getData());
        outState.putSerializable(KEY_LAST_ANIMATED_POS, adapter.getLastAnimatedPosition());
    }

    private void setupLayoutManager() {
        GridLayoutManager layoutMgr = new GridLayoutManager(getActivity(), 2);

        layoutMgr.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int viewType = adapter.getItemViewType(position);

                if (viewType == R.id.type_post) return 1; //Post
                else return 2; //Banner
            }
        });

        recyclerView.setLayoutManager(layoutMgr);
    }

    @Override
    public void onBannerClick(int position, ImageView img) {
        Intent i = new Intent(getActivity(), SlideshowActivity.class);
        Data.BannerData data = (Data.BannerData) adapter.getData().get(position);

        int[] coordinates = new int[2];
        img.getLocationOnScreen(coordinates);

        i.putExtra(SlideshowActivity.KEY_URLS, data.bannerImgUrls);
        i.putExtra(SlideshowActivity.KEY_LEFT, coordinates[0]);
        i.putExtra(SlideshowActivity.KEY_TOP, coordinates[1]);
        i.putExtra(SlideshowActivity.KEY_WIDTH, img.getWidth());
        i.putExtra(SlideshowActivity.KEY_HEIGHT, img.getHeight());
        startActivity(i);
        getActivity().overridePendingTransition(0, 0);
    }

    public class ContentScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (scrollListener != null) scrollListener.onScroll(recyclerView, dx, dy);
        }
    }
}
