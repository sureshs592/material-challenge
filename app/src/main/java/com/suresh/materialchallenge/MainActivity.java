package com.suresh.materialchallenge;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.suresh.materialchallenge.sample.content.ContentFragment;
import com.suresh.materialchallenge.ui.widget.FloatingButton;
import com.suresh.materialchallenge.ui.widget.SlidingTabLayout;

import java.util.List;

public class MainActivity extends ActionBarActivity implements ContentFragment.ListScrollListener,
        View.OnLayoutChangeListener, View.OnClickListener {

    SlidingTabLayout tabBar;
    ViewPager viewPager;
    Toolbar toolbar;
    ImageView imgHeader;
    FloatingButton btnFab;

    /*
     * Values for expanding/collapsing the header + toolbar transparency
     */
    int currentHeaderHeight = -1, maxHeaderHeight = -1, minHeaderHeight = -1, scrollY = 0;
    boolean pendingLayoutUpdate = false;
    int a, r, g, b; //toolbar background colour

    /*
     * Values for floating button visibility + animation
     */
    private static final int ANIMATION_DURATION = 150;
    private boolean fabVisible = true;

    /*
     * savedInstanceState bundle keys
     */
    private static final String KEY_CURRENT_HEIGHT = "current_height";
    private static final String KEY_SCROLL_Y = "scroll_y";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.theme_text_contrast));
        toolbar.setTitle(R.string.action_bar_title);
        toolbar.addOnLayoutChangeListener(this); //To get minimum header height
        setSupportActionBar(toolbar);

        int toolbarBg = getResources().getColor(R.color.theme_primary);
        a = 0;
        r = Color.red(toolbarBg);
        g = Color.green(toolbarBg);
        b = Color.blue(toolbarBg);

        imgHeader = (ImageView) findViewById(R.id.imgHeader);
        imgHeader.addOnLayoutChangeListener(this); //To get maximum header height
        tabBar = (SlidingTabLayout) findViewById(R.id.tabBar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        btnFab = (FloatingButton) findViewById(R.id.btnFab);
        btnFab.setOnClickListener(this);

        ContentPagerAdapter pagerAdapter = new ContentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);

        setupTabBar();
        setupHeader(savedInstanceState);
    }

    private void setupTabBar() {
        tabBar.setDistributeEvenly(true);
        tabBar.setCustomTabView(R.layout.tab_layout, R.id.tvTabTitle);
        tabBar.setSelectedIndicatorColors(getResources().getColor(R.color.theme_accent));
        tabBar.setViewPager(viewPager);
    }

    private void setupHeader(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            currentHeaderHeight = savedInstanceState.getInt(KEY_CURRENT_HEIGHT);
            scrollY = savedInstanceState.getInt(KEY_SCROLL_Y);
            pendingLayoutUpdate = true;
        }

        String imageUrl = "https://dl.dropboxusercontent.com/u/12858073/Material%20Challenge%20Images/Banner%20Imgs/header.jpg";
        List<Bitmap> cachedImgs = MemoryCacheUtils.findCachedBitmapsForImageUri(imageUrl,
                ImageLoader.getInstance().getMemoryCache());

        if (!cachedImgs.isEmpty()) {
            imgHeader.setImageBitmap(cachedImgs.get(0));
        } else {
            ImageLoader.getInstance().displayImage(imageUrl, imgHeader);
        }
    }

    @Override
    public void onScroll(RecyclerView recyclerView, int dx, int dy) {
        if (dy == 0) return; //onScroll() gets called dy = 0 at the start of the activity. Can be ignored.

        scrollY += dy;
        boolean isHeaderScrolling = scrollY > (maxHeaderHeight - minHeaderHeight);

        if (!isHeaderScrolling) {
            int newHeight;

            if (dy < 0) { //Scroll up
                newHeight = Math.min(currentHeaderHeight - dy, maxHeaderHeight);
            } else { //Scroll down
                newHeight = Math.max(currentHeaderHeight - dy, minHeaderHeight);
            }

            if (newHeight != currentHeaderHeight) updateHeader(newHeight);

        } else {
            //Need to check if the header is collapsed completely
            int diffFromMin = currentHeaderHeight - minHeaderHeight;

            if (diffFromMin > 0) { //Not completely collapsed
                updateHeader(minHeaderHeight);
            }

            //Now we need to control floating action button visibility
            controlFloatingButtonVisibility(dy);
        }
    }

    private void controlFloatingButtonVisibility(int dy) {
        if (dy > 0 && fabVisible) { //Scroll down. Hide
            fabVisible = false;
            btnFab.animate()
                    .setDuration(ANIMATION_DURATION)
                    .translationY(btnFab.getHeight());
        } else if (dy < 0 && !fabVisible) { //Scroll up. Show
            fabVisible = true;
            btnFab.animate()
                    .setDuration(ANIMATION_DURATION)
                    .translationY(0);
        }
    }

    private void updateHeader(int newHeight) {
        //Header expand/collapse
        imgHeader.getLayoutParams().height = newHeight;
        currentHeaderHeight = newHeight;
        imgHeader.requestLayout();

        //Toolbar background transparency
        float diff = maxHeaderHeight - newHeight;
        float maxDiff = maxHeaderHeight - minHeaderHeight;
        float ratio = diff / maxDiff;
        a = (int) (ratio * 255);
        toolbar.setBackgroundColor(Color.argb(a, r, g, b));
    }

    @Override
    public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        if (v.getId() == R.id.imgHeader) { //Listening to image view to get the max possible height of the header section
            maxHeaderHeight = bottom - top;

            //Set the current header height if it's not initialised yet (can be initialised from stored value in savedInstanceState)
            if (currentHeaderHeight == -1) currentHeaderHeight = maxHeaderHeight;

            v.removeOnLayoutChangeListener(this); //Have got the initial height. Hence removing the listener.

        } else if (v.getId() == R.id.toolbar) { //Listening to toolbar to get the min possible height of the header section
            minHeaderHeight = bottom - top;

            v.removeOnLayoutChangeListener(this); //Have got the initial height. Hence removing the listener.
        }

        //Checking if there's a pending layout update and if we have enough information to execute it
        if (maxHeaderHeight != -1 && minHeaderHeight != -1 && pendingLayoutUpdate) {
            Log.v("test", "Pending UI Update. Values: current=" + currentHeaderHeight + ", min=" + minHeaderHeight + ", max=" + maxHeaderHeight);
            //Posting to the main thread to prevent interfering with the current layout pass (else onRequestLayout() messes up the UI)
            imgHeader.post(new Runnable() {
                @Override
                public void run() {
                    updateHeader(currentHeaderHeight);
                    pendingLayoutUpdate = false;
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(KEY_CURRENT_HEIGHT, currentHeaderHeight);
        outState.putInt(KEY_SCROLL_Y, scrollY);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "Floating action button clicked!", Toast.LENGTH_SHORT).show();
    }

    private class ContentPagerAdapter extends FragmentPagerAdapter {

        private String[] tabTitles;

        public ContentPagerAdapter(FragmentManager fm) {
            super(fm);
            tabTitles = new String[] { "TAB ONE", "TAB TWO" };
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            return new ContentFragment();
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
