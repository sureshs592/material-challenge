package com.suresh.materialchallenge.utils;

import com.suresh.materialchallenge.utils.Data;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by suresh on 26/3/15.
 */
public class SampleDataGenerator {

    private ArrayList<Data> list;
    private String[] profileImgUrls;
    private String[] bannerImgUrls;
    private Random r;

    public SampleDataGenerator(int size) {
        list = new ArrayList<Data>();
        r = new Random();

        profileImgUrls = new String[] {
                "https://dl.dropboxusercontent.com/u/12858073/material%20challenge%20images/Profile%20Pics/1.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/material%20challenge%20images/Profile%20Pics/2.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/material%20challenge%20images/Profile%20Pics/3.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/material%20challenge%20images/Profile%20Pics/4.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/material%20challenge%20images/Profile%20Pics/5.jpg"
        };
        bannerImgUrls = new String[] {
                "https://dl.dropboxusercontent.com/u/12858073/Material%20Challenge%20Images/Banner%20Imgs/banner_1.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/Material%20Challenge%20Images/Banner%20Imgs/banner_2.jpg",
                "https://dl.dropboxusercontent.com/u/12858073/Material%20Challenge%20Images/Banner%20Imgs/banner_3.jpg"
        };

        initList(size);
    }

    private void initList(int size) {
        for (int i = 0; i < size; i++) {
            float value = r.nextFloat();

            if (value >= 0.1) {
                Data.PostData d = new Data.PostData();
                d.profileImgUrl = generateImageUrl();
                list.add(d);
            } else {
                Data.BannerData d = new Data.BannerData();
                d.bannerImgUrls = bannerImgUrls;
                list.add(d);
            }

        }
    }

    private String generateImageUrl() {
        return profileImgUrls[r.nextInt(profileImgUrls.length)];
    }

    public ArrayList<Data> getList() {
        return list;
    }


}
