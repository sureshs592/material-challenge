package com.suresh.materialchallenge.utils;

import java.io.Serializable;

/**
 * Created by suresh on 28/3/15.
 */
public abstract class Data implements Serializable {
    public static class PostData extends Data implements Serializable {
        public String profileImgUrl;
    }

    public static class BannerData extends Data implements Serializable {
        public String[] bannerImgUrls;
    }
}
